﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DAL;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public ProjectController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [Route("create")]
        [HttpPost]
        public ActionResult<decimal> Create([FromBody] DTO.Project project)
        {
            try
            {
                _unitOfWork.Projects.Create(project);
                return Created("/api/user/create", project);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("read")]
        [HttpGet]
        public ActionResult<int> Read()
        {
            return Ok(_unitOfWork.Projects.GetAll());
        }

        [Route("update")]
        [HttpPut]
        public ActionResult<int> Update([FromBody] DTO.Project project)
        {
            if (_unitOfWork.Projects.Get(project.Id) == null)
            {
                return BadRequest("Id not valid");
            }
            try
            {
                _unitOfWork.Projects.Update(project);
                return Ok(_unitOfWork.Tasks.Get(project.Id));
            }
            catch (ArgumentException e)
            {
                if (e.Message == "Vehicle not found")
                {
                    return NotFound(e.Message);
                }
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public ActionResult<int> Delete(int id)
        {
            if (_unitOfWork.Projects.Get(id) == null)
            {
                return BadRequest("Id not valid");
            }
            try
            {
                _unitOfWork.Projects.Delete(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
