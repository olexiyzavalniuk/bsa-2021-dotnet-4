﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DAL;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public TeamController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [Route("create")]
        [HttpPost]
        public ActionResult<decimal> Create([FromBody] DTO.Team team)
        {
            try
            {
                _unitOfWork.Teams.Create(team);
                return Created("/api/user/create", team);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("read")]
        [HttpGet]
        public ActionResult<int> Read()
        {
            return Ok(_unitOfWork.Teams.GetAll());
        }

        [Route("update")]
        [HttpPut]
        public ActionResult<int> Update([FromBody] DTO.Team team)
        {
            if (_unitOfWork.Teams.Get(team.Id) == null)
            {
                return BadRequest("Id not valid");
            }
            try
            {
                _unitOfWork.Teams.Update(team);
                return Ok(_unitOfWork.Teams.Get(team.Id));
            }
            catch (ArgumentException e)
            {
                if (e.Message == "Vehicle not found")
                {
                    return NotFound(e.Message);
                }
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public ActionResult<int> Delete(int id)
        {
            if (_unitOfWork.Teams.Get(id) == null)
            {
                return BadRequest("Id not valid");
            }
            try
            {
                _unitOfWork.Teams.Delete(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
