﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.BLL;
using WebAPI.DTO;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LinqQueryController : Controller
    {
        private readonly Query _query = new Query();


        [Route("First/{id}")]
        [HttpGet]
        public ActionResult<Dictionary<Hierarchy, int>> One(int id)
        {
            return Ok(First.Run(id));
        }

        [Route("Second/{id}")]
        [HttpGet]
        public ActionResult<List<Task2>> Two(int id)
        {
            return Ok(Second.Run(id));
        }

        [Route("Third/{id}")]
        [HttpGet]
        public ActionResult<List<DTO.Third>> Three(int id)
        {
            return Ok(BLL.Third.Run(id));
        }
    }
}
