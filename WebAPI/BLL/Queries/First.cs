﻿using System.Collections.Generic;
using WebAPI.DTO;
using WebAPI.DAL;
using System.Linq;

namespace WebAPI.BLL
{
    public static class First
    {
        public static Dictionary<Hierarchy, int> Run(int id)
        {
            Dictionary<Hierarchy, int> result;
            result = Query.Hierarchies.Where(hierarchy => hierarchy.Tasks.Any(task => task.Performer.Id == id)).
                ToDictionary(
                h => h,
                h => h.Tasks.Where(task => task.Performer.Id == id).Count()
                );

            return result;
        }
    }
}
