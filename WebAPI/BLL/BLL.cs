﻿using System;
using System.Collections.Generic;
using WebAPI.DTO;
using WebAPI.DAL;
using System.Linq;

namespace WebAPI.BLL
{
    public class Query
    {
        private List<Project> _projects;
        private List<Task> _tasks;
        private List<Team> _teams;
        private List<User> _users;
        public static List<Hierarchy> Hierarchies { get; private set; }

        public Query()
        {
            var unitOfWork = new UnitOfWork();
            _projects = (List<Project>)unitOfWork.Projects.GetAll();
            _tasks = (List<Task>)unitOfWork.Tasks.GetAll();
            _teams = (List<Team>)unitOfWork.Teams.GetAll();
            _users = (List<User>)unitOfWork.Users.GetAll();

            Hierarchies = _projects.GroupJoin(_tasks, project => project.Id,
                task => task.ProjectId, (project, tasks) =>
            new Hierarchy
            {
                Id = project.Id,
                Name = project.Name,
                Description = project.Description,
                Deadline = project.Deadline,
                Tasks = tasks.Where(task => task.ProjectId == project.Id)
                .Join(_users, task => task.PerformerId, user => user.Id, (task, user) =>
                    new Task2
                    {
                        Id = task.Id,
                        Name = task.Name,
                        Description = task.Description,
                        State = task.State,
                        CreatedAt = task.CreatedAt,
                        FinishedAt = task.FinishedAt,
                        Performer = _users.First(user => user.Id == task.PerformerId)
                    }).ToList(),
                Team = _teams.First(team => team.Id == project.TeamId),
                Author = _users.First(user => user.Id == project.AuthorId)
            }).ToList();
        }
    }
}