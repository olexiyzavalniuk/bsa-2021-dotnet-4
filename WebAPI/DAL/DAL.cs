﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WebAPI.DTO;

namespace WebAPI.DAL
{
    interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
    }


    public class UserRepository : IRepository<User>
    {
        private List<User> _users;

        public UserRepository()
        {
            _users = JsonConvert.DeserializeObject<IEnumerable<User>>
                (File.ReadAllText("Data/Users.json")).ToList();
        }

        public void Create(User item)
        {
            _users.Add(item);
        }

        public void Delete(int id)
        {
            var item = _users.Single(i => i.Id == id);
            _users.Remove(item);
        }

        public User Get(int id)
        {
            return GetAll().FirstOrDefault(u => u.Id == id);
        }

        public IEnumerable<User> GetAll()
        {
            return _users;
        }

        public void Update(User item)
        {
            Delete(item.Id);
            _users.Add(item);
        }
    }


    public class TeamRepository : IRepository<Team>
    {
        private List<Team> _teams;

        public TeamRepository()
        {
            _teams = JsonConvert.DeserializeObject<IEnumerable<Team>>
                (File.ReadAllText("Data/Teams.json")).ToList();
        }

        public void Create(Team item)
        {
            _teams.Add(item);
        }

        public void Delete(int id)
        {
            var item = _teams.Single(i => i.Id == id);
            _teams.Remove(item);
        }

        public Team Get(int id)
        {
            return GetAll().FirstOrDefault(u => u.Id == id);
        }

        public IEnumerable<Team> GetAll()
        {
            return _teams;
        }

        public void Update(Team item)
        {
            Delete(item.Id);
            _teams.Add(item);
        }
    }


    public class TaskRepository : IRepository<Task>
    {
        private List<Task> _tasks;

        public TaskRepository()
        {
            _tasks = JsonConvert.DeserializeObject<IEnumerable<Task>>
                (File.ReadAllText("Data/Tasks.json")).ToList();
        }

        public void Create(Task item)
        {
            _tasks.Add(item);
        }

        public void Delete(int id)
        {
            var item = _tasks.Single(i => i.Id == id);
            _tasks.Remove(item);
        }

        public Task Get(int id)
        {
            return GetAll().FirstOrDefault(u => u.Id == id);
        }

        public IEnumerable<Task> GetAll()
        {
            return _tasks;
        }

        public void Update(Task item)
        {
            Delete(item.Id);
            _tasks.Add(item);
        }
    }


    public class ProjectRepository : IRepository<Project>
    {
        private List<Project> _projects;

        public ProjectRepository()
        {
            _projects = JsonConvert.DeserializeObject<IEnumerable<Project>>
                (File.ReadAllText("Data/Projects.json")).ToList();
        }

        public void Create(Project item)
        {
            _projects.Add(item);
        }

        public void Delete(int id)
        {
            var item = _projects.Single(i => i.Id == id);
            _projects.Remove(item);
        }

        public Project Get(int id)
        {
            return GetAll().FirstOrDefault(u => u.Id == id);
        }

        public IEnumerable<Project> GetAll()
        {
            return _projects;
        }

        public void Update(Project item)
        {
            Delete(item.Id);
            _projects.Add(item);
        }
    }

    public class UnitOfWork
    {
        private UserRepository _userRepository;
        private TeamRepository _teamRepository;
        private TaskRepository _taskRepository;
        private ProjectRepository _projectRepository;

        public UserRepository Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository();
                return _userRepository;
            }
        }

        public TeamRepository Teams
        {
            get
            {
                if (_teamRepository == null)
                    _teamRepository = new TeamRepository();
                return _teamRepository;
            }
        }

        public TaskRepository Tasks
        {
            get
            {
                if (_taskRepository == null)
                    _taskRepository = new TaskRepository();
                return _taskRepository;
            }
        }

        public ProjectRepository Projects
        {
            get
            {
                if (_projectRepository == null)
                    _projectRepository = new ProjectRepository();
                return _projectRepository;
            }
        }
    }

}